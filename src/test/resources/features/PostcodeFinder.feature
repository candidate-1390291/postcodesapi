Feature: Query A Postcode And Receive A 200 response

  Scenario Outline: Verify Lookup PostCode API Endpoint
    Given A request to lookup a postcode "<PostCode>" to postcodes endpoint
    When I send a get request to lookup postcode endpoint
    Then the API call is success with status code <StatusCode> as response

    Examples: 
      | PostCode | StatusCode |
      | SW1P4JA  |        200 |
