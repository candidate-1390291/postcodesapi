package com.postcodefinder.testapiendpoints;

public enum APIEndpoints {
	
	LOOKUP("/postcodes/{postcode}");
	
	private final String resource;
	
	APIEndpoints(String resource)
	{
		this.resource=resource;
	}
	
	public String getResource()
	{
		return resource;
	}
	

}

