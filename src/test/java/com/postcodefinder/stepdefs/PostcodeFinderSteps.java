package com.postcodefinder.stepdefs;

import com.postcodefinder.testapiendpoints.APIEndpoints;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.rest.SerenityRest;

public class PostcodeFinderSteps extends BaseTest {

	@Given("A request to lookup a postcode {string} to postcodes endpoint")
	public void aRequestToTheLookupPostcodesEndpoint(String postCode) {
		postCodeReq(postCode);
	}

	@When("I send a get request to lookup postcode endpoint")
	public void sendAGETRequestToLookUpPostCodeendpoint() {
		SerenityRest
		.when()
		.get(APIEndpoints.LOOKUP.getResource());
	}

	@Then("the API call is success with status code {int} as response")
	public void the_API_call_is_success_with_status_code_as_response(Integer statusCode) {
		SerenityRest.then().statusCode(statusCode);
	}
	
	private void postCodeReq(String postCode) {
		SerenityRest
		.given()
		.baseUri(BASE_URI)
		.pathParam("postcode", postCode);
	}

}
