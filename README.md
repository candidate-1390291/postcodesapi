PROBLEM STATEMENT: 
Application under test: Postcodes Api
Feature –  Query a postcode and receive a 200 response

Scenario:

When I send a get request to api.postcodes.io/postcodes/SW1P4JA
Then I get a 200 response


Steps To Follow:

1) In Terminal navigate to any folder and clone the repo

git clone git@gitlab.com:candidate-1390291/postcodesapi.git

2) Through Terminal Navigate to the folder "postcodesapi" 

3) Input the following command

mvn clean verify serenity:aggregate

4) Wait For Build Success message in the terminal,

5) Navigate to the following location and open index.html to verify the test execution report

/postcodesapi/target/site/serenity/index.html